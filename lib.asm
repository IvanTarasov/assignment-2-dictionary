global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_err

section .text

exit:
	mov rax, 60
	xor rdi, rdi
	syscall

print_newline:
	mov rax, 10
	push rax
	mov rsi, rsp
	mov rdi, 1
	mov rdx, 1
	mov rax, 1
	syscall
	pop rax
	ret

string_length:
	mov rax, -1
	.count:
		inc rax
		cmp byte[rdi + rax], 0	; если в строке не встретился нуль-терминатор,
		jne .count		; то начинаю цикл заново
	ret

print_string:
	push rdi			; сохраняю значение регистра
	call string_length		; узнаю длину строки			
	pop rdi
	mov rsi, rdi
	mov rdx, rax
	mov rdi, 1
	mov rax, 1
	syscall
	ret

print_char:
	push rdi
	mov rdx, 1
	mov rdi, 1
	mov rax, 1
	mov rsi, rsp
	syscall
	pop rdi
	ret

print_uint:
	push rbx
	push rdi
	mov rax, rdi
	mov rbx, 10
	test r12, r12
	.div:
		inc r12		;делю на 10, остаток сохраняю в стек
		mov rdx, 0
		div rbx
		push rdx
		test rax, rax
		jnz .div
	.print:
		dec r12		;извлекаю остатки из стека. Из них формирую и вывожу на экран число
		pop rdi
		add rdi, "0"
		call print_char
		cmp r12, 0
		jnz .print
	pop rdi
	pop rbx
	ret

print_int:
	cmp rdi, 0
	jns print		;если число отрицательное, приписываю минус и меняю знак числа.
	neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	print:
		call print_uint	;дальше все то же самое, что и с print_uint
		ret

string_equals:
	push r12
	mov rax, -1
	.loop:
		inc rax
		mov r12b, byte[rdi + rax] 	;если встретился 0-терминатор, перехожу в .end
		test r12, r12
		jz .end
		cmp r12b, byte[rsi + rax]	;если символы равны, продолжаю проверять
		jz .loop
		jmp .not
	.end: 
		cmp r12b, byte[rsi + rax]	;если 0-терминаторы у обоих строк, то строки равны
		jnz .not
		mov rax, 1
		jmp .ret
		.not: mov rax, 0
		.ret: 
			pop r12
			ret

read_char:
	mov rax, 0
	mov rdi, 0
	dec rsp
	mov rsi, rsp
	mov rdx, 1
	syscall
	test rax, rax
	je .end
	mov al, [rsp]
	.end:    
	    inc rsp
	    ret 

read_word:
	push r12
	push r13		; счетчик символов
	push r14		; начало буфера
	mov r13, 0
	mov r14, rdi
	mov r12, rsi
	.spaces:
		call read_char		;пропускаю пробелы
		cmp rax, 0x20		
		jz .space
		cmp rax, 0x9
		jz .space
		cmp rax, 0xA
		jz .space
	.loop:	
		cmp rax, 0xA
		jz .end
		mov [r14 + r13], al
		cmp r13, r12
		jz .not
		inc r13
		jmp .spaces
	.space:
		test r13, r13
		jz .spaces
		jmp .loop
	.not: 	mov rax, 0		;если строка больше размера буфера, то записываю в акк. 0
		jmp .ret
	.end: 	
		mov byte[r14 + r13], 0
		mov rax, r14
		mov rdx, r13
	.ret:	
		pop r14
		pop r13
		pop r12
		ret


parse_uint:
	push r8
	push r9
	mov rax, 0
	mov rsi, 0
	mov r9, 10
	.loop:
		mov r8b, byte[rdi + rsi]
		cmp r8, 0
		jz .number
		sub r8, "0"		;у каждого символа убираю символ "0", превращая символ в
		cmp r8, 10		;число
		jns .not_number
		cmp r8, -1
		js .not_number
		mul r9
		add rax, r8
		inc rsi
		jmp .loop
	.not_number:
		test rsi, rsi	;если первый символ не число, то спарсить невозможно, иначе имеют-
		jnz .number	;ся числа в начале строки
		mov rax, 0	;если в невозможно спарсить строку, записываю 0 в аккумулятор
		jmp .ret
	.number:
		mov rdx, rsi
	.ret:
		pop r9
		pop r8
		ret

parse_int:
	push r12
	mov r12, 0
	mov sil, byte[rdi]
	cmp rsi, 45
	jnz .parse
	inc rdi
	inc r12			;если в начале стоит минус, записываю в буфер (r10), для того, что-
	.parse:			;бы после парсинга строки изменить знак у числа (neg)
		call parse_uint
	cmp r12, 1
	jnz .ret
	neg rax			;меняю знак, если в начале строки стоял минус
	inc rdx
	.ret: 
		pop r12
		ret

string_copy:
	mov rax, -1
	push r12
	.copy:
		inc rax
		cmp rax, rdx			;проверка на переполнение
		jz .overflow
		mov r12b, byte[rdi+rax]		;копирую по одному символу
		mov byte[rsi+rax], r12b
		test r12, r12
		jnz .copy
		jmp .ret
	.overflow:
		mov rax, 0
	.ret: 
		pop r12
		ret

print_err:
	push rdi			; сохраняю значение регистра
	call string_length		; узнаю длину строки			
	pop rdi
	mov rsi, rdi
	mov rdx, rax
	mov rdi, 2
	mov rax, 1
	syscall
	ret
