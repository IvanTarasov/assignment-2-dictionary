from subprocess import Popen, PIPE

inputs = ["first", "hello world", "!Hai!", "fourth", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"]
outputs = ["first word", "second word", "third word", "", "255 symbols"]
errors = ["", "", "", "Error: Key is not found\n", ""]

for i in range(len(inputs)):
	p = Popen("./main", shell=None, stdin=PIPE, stdout=PIPE, stderr=PIPE)
	inp = inputs[i]
	out = outputs[i]
	err = errors[i]
	data = p.communicate(inp + "\n")
	if data[0] == out and data[1] == err:
		print("Test passed with input: \"" + inp + "\"")
	else:
		print("Test failed - stdout: \"" + data[0] + "\", stderr: \"" + data[1] + "\". Expected stdout: \"" + out + "\", stderr: \"" + err + "\"")
