ASM=nasm -felf64

lib.o: lib.asm
	$(ASM) -o lib.o lib.asm

dict.o: dict.asm
	$(ASM) -o dict.o dict.asm

main.o: main.asm lib.o dict.o
	$(ASM) -o main.o main.asm

program: main.o lib.o dict.o
	ld -o main main.o lib.o dict.o
	rm *.o

test:
	python test.py
