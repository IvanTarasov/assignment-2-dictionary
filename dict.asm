%include "lib.inc"
global find_word
global get_value

section .text

find_word:
	lea r8, [rsi]
	.loop:
		lea rsi, [r8 + 8]
		call string_equals
		test rax, rax
		jz .next
		mov rax, r8
		jmp .ret
	.next:
		mov r8, [r8]
		test r8, r8
		jz .not_found
		jmp .loop
	.not_found:
		mov rax, 0
	.ret:
		ret

get_value:
	add rdi, 8				;пропускаю место, где храниться адрес следующего вхождения
	call string_length		;узнаю длину ключа, чтобы также пропустить его
	add rdi, rax
	inc rdi
	mov rax, rdi
	ret
