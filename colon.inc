%define last_label 0

%macro colon 2
	%2: dq last_label
	db %1, 0
	%define last_label %2

%endmacro
