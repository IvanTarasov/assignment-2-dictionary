%include "lib.inc"
%include "words.inc"
%include "dict.inc"

section .bss
key: resb 256

section .rodata
not_found: db "Error: Key is not found", 10, 0

section .text
global _start

_start:
	mov rdi, key
	mov rsi, 256
	call read_word
	mov rdi, rax
	mov rsi, first
	call find_word
	test rax, rax
	jnz .found
	mov rdi, not_found
	call print_err
	jmp .exit

.found:
	mov rdi, rax
	call get_value
	mov rdi, rax
	call print_string
.exit:
	call exit
